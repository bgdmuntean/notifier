﻿using System;
using System.Collections.Generic;
using System.Text;
using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Domain.Models.v1;
using Notifier.Publisher.Domain.Models.v1.ValueObjects;
using Xunit;

namespace Notifier.Publisher.UnitTests.Models.v1
{
    public class NotificationTests
    {
        [Fact]
        public void Notification_ShouldSucceed_WhenTemperatureIsValid()
        {
            // Arrange & Act
            var notification = new Notification("test", DateTimeOffset.Parse("2020-01-01"), 12.3, "Copenhagen", "Clear", Level.Critical);
            
            // Assert
            Assert.NotNull(notification);
            Assert.Equal("test", notification.Message);
            Assert.Equal(DateTimeOffset.Parse("2020-01-01"), notification.Timestamp);
            Assert.Equal(12.3, notification.TemperatureCelsius.Value);
            Assert.Equal("Copenhagen", notification.City);
            Assert.Equal("Clear", notification.Condition);
            Assert.Equal(Level.Critical, notification.Level);
        }

        [Fact]
        public void Notification_ShouldThrowTemperatureOutsideOfBounds_WhenTemperatureIsInvalid()
        {
            // Arrange & Act & Assert
            Assert.Throws<TemperatureOutsideOfBoundsException>(() =>
            {
                var notification = new Notification("test", DateTimeOffset.Parse("2020-01-01"), 100, "Copenhagen",
                    "Clear", Level.Critical);
            });
        }
    }
}
