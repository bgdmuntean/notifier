﻿using System;
using System.Text;
using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Domain.Models.v1;
using Notifier.Publisher.Infrastructure.Utils.v1.Extensions;
using Xunit;

namespace Notifier.Publisher.UnitTests.Infrastructure.Extensions.v1
{
    public class MessageExtensionsTests
    {
        [Fact]
        public void GetRabbitMqMessage_ShouldSucceed_WhenNonNullObjectIsReceived()
        {
            // Arrange 
            var notification = new Notification("test", DateTimeOffset.Parse("2020-01-01"), 12.3, "Copenhagen", "Clear", Level.Critical);


            // Act

            var encoded = notification.GetRabbitMqMessage();

            // Assert
            Assert.NotNull(encoded);
        }

        [Fact]
        public void GetRabbitMqMessage_ShouldSucceed_WhenNonNullPrimitiveObjectIsReceived()
        {
            // Arrange & Act
            var encoded1 = "test".GetRabbitMqMessage();

            // Assert
            Assert.NotNull(encoded1);
            Assert.Equal(Encoding.UTF8.GetBytes("test").Length, encoded1.Length);
        }



        [Fact]
        public void GetRabbitMqMessage_ShouldThrowMessageEncodingException_WhenNullObjectIsReceived()
        {
            // Arrange & Act & Assert
            Assert.Throws<MessageEncodingException>(() =>
            {
                Notification notification = null;
                var encoded = notification.GetRabbitMqMessage();
            });
        }
    }
}
