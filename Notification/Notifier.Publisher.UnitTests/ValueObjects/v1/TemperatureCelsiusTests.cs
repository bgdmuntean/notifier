﻿using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Domain.Models.v1.ValueObjects;
using Xunit;

namespace Notifier.Publisher.UnitTests.ValueObjects.v1
{
    public class TemperatureCelsiusTests
    {
        [Fact]
        public void TemperatureCelsiusValueObject_ShouldSucceed_WhenTemperatureIsValid()
        {
            // Arrange & Act
            var temperatureValueObject = TemperatureCelsius.From(12.3);

            // Assert
            Assert.NotNull(temperatureValueObject);
        }

        [Fact]
        public void TemperatureCelsiusValueObject_ShouldSucceed_WhenTemperatureIsValidAtBounds()
        {
            // Arrange & Act
            var temperatureAValueObject = TemperatureCelsius.From(56.7);
            var temperatureBValueObject = TemperatureCelsius.From(-273);

            // Assert


            Assert.NotNull(temperatureAValueObject);
            Assert.Equal(TemperatureCelsius.From(56.7), temperatureAValueObject);

            Assert.NotNull(temperatureBValueObject);
            Assert.Equal(TemperatureCelsius.From(-273), temperatureBValueObject);
        }

        [Fact]
        public void TemperatureCelsiusValueObject_ShouldThrowTemperatureOutOfBoundsException_WhenTemperatureIsInvalid()
        {
            // Arrange & Act && Assert

            Assert.Throws<TemperatureOutsideOfBoundsException>(() => { TemperatureCelsius.From(60); });
            Assert.Throws<TemperatureOutsideOfBoundsException>(() => { TemperatureCelsius.From(-300); });
        }
    }
}