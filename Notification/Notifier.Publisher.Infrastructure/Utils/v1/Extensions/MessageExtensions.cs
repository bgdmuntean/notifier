﻿using System.Text;
using Newtonsoft.Json;
using Notifier.Publisher.Domain.Core.v1.Exceptions;

namespace Notifier.Publisher.Infrastructure.Utils.v1.Extensions
{
    public static class MessageExtensions
    {
        public static byte[] GetRabbitMqMessage<T>(this T notification)
        {
            if (notification is null)
                throw new MessageEncodingException("Unable to convert the message in a stream of bytes - the object is null");
            
            var type = typeof(T);

            if (type.IsPrimitive || type.IsValueType || type == typeof(string))
                return Encoding.UTF8.GetBytes(notification.ToString() ?? string.Empty);

            var serializedObj = JsonConvert.SerializeObject(notification);
            return Encoding.UTF8.GetBytes(serializedObj);

        }
    }
}