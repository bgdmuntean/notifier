﻿namespace Notifier.Publisher.Infrastructure.Utils.v1.Constants
{
    public static class RabbitMq
    {
        public const string Username = "RabbitMq:Username";
        public const string Password = "RabbitMq:Password";
        public const string Hostname = "RabbitMq:Hostname";
        public const string Port = "RabbitMq:Port";
    }
}
