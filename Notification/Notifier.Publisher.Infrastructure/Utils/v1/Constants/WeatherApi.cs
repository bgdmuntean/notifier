﻿namespace Notifier.Publisher.Infrastructure.Utils.v1.Constants
{
    public static class WeatherApi
    {
        public const string Endpoint = "/v1/current.json";
        public static class QueryParams
        {
            public const string SearchQuery = "q";
            public const string ApiKey = "key";
        }

        public static class Configuration
        {
            public const string Uri = "WeatherApi:Uri";
            public const string Key = "WeatherApi:Key";

        }
    }
}