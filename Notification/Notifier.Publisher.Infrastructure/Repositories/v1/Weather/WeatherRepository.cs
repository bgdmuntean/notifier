﻿using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Contracts.WeatherApi.v1;
using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Infrastructure.Utils.v1.Constants;
using RestSharp;

namespace Notifier.Publisher.Infrastructure.Repositories.v1.Weather
{
    public class WeatherRepository : IWeatherRepository
    {
        private readonly IConfigurationManagerRepository _configuration;
        private readonly IRestClient _client;

        public WeatherRepository(IConfigurationManagerRepository configuration)
        {
            _configuration = configuration;
            _client = new RestClient(_configuration.GetValue(WeatherApi.Configuration.Uri));
        }

        public async Task<WeatherInformation> GetWeatherForCityAsync(string cityName, CancellationToken token)
        {
            var request = new RestRequest(WeatherApi.Endpoint, Method.GET)
                .AddQueryParameter(WeatherApi.QueryParams.ApiKey, _configuration.GetValue(WeatherApi.Configuration.Key))
                .AddQueryParameter(WeatherApi.QueryParams.SearchQuery, cityName);

            var response = await _client
                .ExecuteAsync<WeatherInformation>(request, token)
                .ConfigureAwait(false);

            if (!response.IsSuccessful)
            {
                throw new WeatherApiException(response.StatusCode, response.Content);
            }
            return JsonConvert.DeserializeObject<WeatherInformation>(response.Content);
        }
    }
}