﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.RabbitMq;
using Notifier.Publisher.Domain.Core.v1;
using Notifier.Publisher.Infrastructure.Utils.v1.Constants;
using Notifier.Publisher.Infrastructure.Utils.v1.Extensions;
using RabbitMQ.Client;

namespace Notifier.Publisher.Infrastructure.Repositories.v1.RabbitMqPublisher
{
    public class RabbitMqPublisherRepository : IPublisherRepository
    {
        private readonly IConfigurationManagerRepository _configuration;

        private readonly string _username;
        private readonly string _password;
        private readonly string _hostname;
        private readonly int _port;

        public RabbitMqPublisherRepository(IConfigurationManagerRepository configuration)
        {
            _configuration = configuration;
            _username = _configuration.GetValue(RabbitMq.Username);
            _password = _configuration.GetValue(RabbitMq.Password);
            _hostname = _configuration.GetValue(RabbitMq.Hostname);
            _port = Convert.ToInt32(_configuration.GetValue(RabbitMq.Port));
        }

        public async Task PublishMessageAsync<T>(string topic, string routingKey, T message, CancellationToken token)
        {
            var factory = CreateConnectionFactory();

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.ExchangeDeclare(topic, ExchangeType.Fanout, true);

            var encodedMessage = message.GetRabbitMqMessage();
            
            channel.BasicPublish(
                exchange: topic,
                routingKey: routingKey,
                basicProperties: null,
                body: encodedMessage);
        }

        private ConnectionFactory CreateConnectionFactory()
        {
            return new ConnectionFactory
            {
                HostName = _hostname,
                UserName = _username,
                Password = _password,
                Port = _port,
            };
        }
    }
}