﻿using System.Linq;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Domain.Core.v1;

namespace Notifier.Publisher.Infrastructure.Repositories.v1.ConfigurationWrapper
{
    public class ConfigurationManagerRepository : IConfigurationManagerRepository
    {

        public string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public bool HasKey(string key)
        {
            return ConfigurationManager.AppSettings.GetChildren().Any(_ => string.Equals(_.Key, key));
        }
    }
}