﻿using System;
using Notifier.Publisher.Domain.Models.v1.ValueObjects;

namespace Notifier.Publisher.Domain.Models.v1
{
    public class Notification
    {
        public Notification(string message, DateTimeOffset timestamp, double temperatureCelsius, string city, string condition, Level level)
        {
            Message = message;
            Timestamp = timestamp;
            TemperatureCelsius = TemperatureCelsius.From(temperatureCelsius);
            City = city;
            Condition = condition;
            Level = level;
        }

        public string Message { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public TemperatureCelsius TemperatureCelsius { get; set; }
        public string City { get; set; }
        public string Condition { get; set; }
        public Level Level { get; set; }
    }
}