﻿using Notifier.Publisher.Domain.Core.v1.Exceptions;
using ValueOf;

namespace Notifier.Publisher.Domain.Models.v1.ValueObjects
{
    public class TemperatureCelsius : ValueOf<double, TemperatureCelsius>
    {
        private const double Max = 56.7;
        private const double Min = -273;

        protected override void Validate()
        {
            if (Value < Min)
            {
                throw new TemperatureOutsideOfBoundsException($"Temperature cannot be bellow {Min}");
            }

            if (Value > Max)
            {
                throw new TemperatureOutsideOfBoundsException($"Temperature cannot be greater than the highest recorded temperature: {Max}");
            }
        }

    }
}
