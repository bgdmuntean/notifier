﻿using System;
using System.Net;
using Newtonsoft.Json;
using Notifier.Publisher.Domain.Core.v1.Exceptions;

namespace Notifier.Publisher.Domain.Core.v1
{
    public class ErrorResponse
    {
        public ErrorResponse(Exception exception)
        {
            StatusCode = HttpStatusCode.InternalServerError;
            Message = string.IsNullOrWhiteSpace(exception.Message) ? JsonConvert.SerializeObject(exception) : exception.Message;
            InternalErrorCode = InternalErrorCode.UndocumentedError;
        }

        public ErrorResponse(BaseException exception)
        {
            StatusCode = exception.StatusCode;
            Message = exception.Message;
            InternalErrorCode = exception.ErrorCode;
        }

        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public InternalErrorCode InternalErrorCode { get; set; }


        public string ToJsonString()
            => JsonConvert.SerializeObject(this);
    }
}