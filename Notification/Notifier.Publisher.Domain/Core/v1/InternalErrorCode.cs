﻿namespace Notifier.Publisher.Domain.Core.v1
{
    public enum InternalErrorCode
    {
        UndocumentedError,
        InvalidTemperatureError,
        WeatherApiError,
        MessageEncodingError
    }
}