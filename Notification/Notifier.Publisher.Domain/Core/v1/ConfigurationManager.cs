﻿using Microsoft.Extensions.Configuration;

namespace Notifier.Publisher.Domain.Core.v1
{
    public class ConfigurationManager
    {
        public static IConfiguration AppSettings { get; set; }
    }
}