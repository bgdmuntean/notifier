﻿using System;
using System.Net;

namespace Notifier.Publisher.Domain.Core.v1.Exceptions
{
    public class TemperatureOutsideOfBoundsException : BaseException
    {
        public TemperatureOutsideOfBoundsException(string message) : base(HttpStatusCode.BadRequest, InternalErrorCode.InvalidTemperatureError, message)
        {
        }

        public TemperatureOutsideOfBoundsException(string message, Exception ex) : base(HttpStatusCode.BadRequest, InternalErrorCode.InvalidTemperatureError, message, ex)
        {
        }
    }
}