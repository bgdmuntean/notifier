﻿using System;
using System.Net;

namespace Notifier.Publisher.Domain.Core.v1.Exceptions
{
    public class WeatherApiException : BaseException
    {
        public WeatherApiException(HttpStatusCode statusCode, string message) : base(statusCode, InternalErrorCode.WeatherApiError, message)
        {
        }

        public WeatherApiException(HttpStatusCode statusCode, string message, Exception ex) : base(statusCode, InternalErrorCode.WeatherApiError, message, ex)
        {
        }
    }
}