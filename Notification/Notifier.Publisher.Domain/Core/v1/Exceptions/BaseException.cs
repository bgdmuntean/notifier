﻿using System;
using System.Net;

namespace Notifier.Publisher.Domain.Core.v1.Exceptions
{
    public abstract class BaseException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public InternalErrorCode ErrorCode { get; set; }

        protected BaseException() : base()
        {

        }

        protected BaseException(HttpStatusCode statusCode, InternalErrorCode errorCode, string message) : base(message)
        {
            StatusCode = statusCode;
            ErrorCode = errorCode;
        }

        protected BaseException(HttpStatusCode statusCode, InternalErrorCode errorCode, string message, Exception ex) : base(message, ex)
        {
            StatusCode = statusCode;
            ErrorCode = errorCode;
        }
    }
}