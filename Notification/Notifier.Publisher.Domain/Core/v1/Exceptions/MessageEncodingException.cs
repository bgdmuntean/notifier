﻿using System;
using System.Net;

namespace Notifier.Publisher.Domain.Core.v1.Exceptions
{
    public class MessageEncodingException : BaseException
    {
        public MessageEncodingException(string message) : base(HttpStatusCode.BadRequest, InternalErrorCode.MessageEncodingError, message)
        {
        }

        public MessageEncodingException(string message, Exception ex) : base(HttpStatusCode.BadRequest, InternalErrorCode.MessageEncodingError, message, ex)
        {
        }
    }
}