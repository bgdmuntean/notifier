﻿namespace Notifier.Subscriber.Domain.Models.v1
{
    public enum Level
    {
        Information,
        Critical,
        Warning
    }
}