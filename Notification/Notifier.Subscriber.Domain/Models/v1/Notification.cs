﻿using System;

namespace Notifier.Subscriber.Domain.Models.v1
{
    public class Notification
    {
        public Notification(string message, DateTimeOffset timestamp, double temperatureCelsius, string city, string condition, Level level)
        {
            Message = message;
            Timestamp = timestamp;
            TemperatureCelsius = temperatureCelsius;
            City = city;
            Condition = condition;
            Level = level;
        }

        public Notification()
        {
            
        }

        public string Message { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public double TemperatureCelsius { get; set; }
        public string City { get; set; }
        public string Condition { get; set; }
        public Level Level { get; set; }

        public string GetLoggingInfo()
        {
            return $"Timestamp: {Timestamp} \nCity: {City} \nTemperature (Celsius): {TemperatureCelsius} \nCondition: {Condition}Logged Message: {Message}";
        }
    }
}