﻿using Microsoft.Extensions.Configuration;

namespace Notifier.Subscriber.Domain.Core.v1
{
    public class ConfigurationManager
    {
        public static IConfiguration AppSettings { get; set; }
    }
}