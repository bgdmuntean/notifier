﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Notifier.Publisher.Contracts.Responses.v1
{
    public class ResponseObject<T>
    {
        public ResponseObject(T item)
        {
            Items = new List<T> {item};
            Count = 1;
        }

        public ResponseObject(ICollection<T> items)
        {
            Items = items;
            Count = items.Count;
        }

        public ResponseObject(ICollection<T> items, string message)
        {
            Items = items;
            Count = items.Count;
            Message = message;
        }

        public ResponseObject()
        {
            Message = "No Items were found that correspond to your search criteria";
        }

        public ICollection<T> Items { get; set; } = new List<T>();
        public int Count { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
    }
}