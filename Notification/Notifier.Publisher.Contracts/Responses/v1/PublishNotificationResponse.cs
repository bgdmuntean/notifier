﻿using System;

namespace Notifier.Publisher.Contracts.Responses.v1
{
    public class PublishNotificationResponse
    {
        public PublishNotificationResponse(string message, DateTimeOffset timestamp, double temperatureCelsius, string city, string condition, string level)
        {
            Message = message;
            Timestamp = timestamp;
            TemperatureCelsius = temperatureCelsius;
            City = city;
            Condition = condition;
            Level = level;
        }

        public string Message { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public double TemperatureCelsius { get; set; }
        public string City { get; set; }
        public string Condition { get; set; }
        public string Level { get; set; }
    }
}