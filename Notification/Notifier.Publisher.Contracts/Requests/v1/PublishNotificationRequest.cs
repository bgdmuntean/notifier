﻿using Notifier.Publisher.Domain.Models.v1;

namespace Notifier.Publisher.Contracts.Requests.v1
{
    public class PublishNotificationRequest
    {
        public Level Level { get; set; }
        public string City { get; set; }
        public string Message { get; set; }
    }
}