﻿namespace Notifier.Publisher.Contracts.WeatherApi.v1
{
    public class WeatherInformation
    {
        public Location Location { get; set; }
        public Current Current { get; set; }
    }
}