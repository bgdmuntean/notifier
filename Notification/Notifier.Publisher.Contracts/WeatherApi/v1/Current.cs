﻿using Newtonsoft.Json;

namespace Notifier.Publisher.Contracts.WeatherApi.v1
{
    public class Current
    {
        [JsonProperty("temp_c")]
        public double Temperature { get; set; }
        public Condition Condition { get; set; }
    }
}