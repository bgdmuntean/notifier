﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Moq;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.RabbitMq;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Application.Services.v1.PublishNotificationService;
using Notifier.Publisher.Contracts.WeatherApi.v1;
using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Domain.Models.v1;
using Xunit;

namespace Notifier.Publisher.IntegrationTests.Services.v1
{
    public class PublishNotificationCommandTests
    {
        private readonly PublishNotificationCommandHandler _sut;

        private readonly Mock<IMediator> _mediator = new Mock<IMediator>();
        private readonly Mock<IConfigurationManagerRepository> _configuration = new Mock<IConfigurationManagerRepository>();
        private readonly Mock<IWeatherRepository> _weatherMock = new Mock<IWeatherRepository>();
        private readonly Mock<IPublisherRepository> _publisherMock = new Mock<IPublisherRepository>();

        public PublishNotificationCommandTests()
        {
            _sut = new PublishNotificationCommandHandler(_publisherMock.Object, _weatherMock.Object, _configuration.Object);
        }

        [Fact]
        public async Task PublishNotificationCommand_ShouldSucceed_WhenValidRequest()
        {
            // Arrange

            var weatherInfo = new WeatherInformation
            {
                Current = new Current
                {
                    Condition = new Condition
                    {
                        Code = 122,
                        Icon = "No Icon",
                        Text = "Chilly"
                    },
                    Temperature = 12.3
                },
                Location = new Location
                {
                    Country = "Denmark",
                    Name = "Copenhagen",
                    Region = "Hovedstaden"
                }
            };

            var notification = new Notification("test", DateTimeOffset.Now, 12.3, "Copenhagen", "Chilly", Level.Critical);

            var command = new PublishNotificationCommand(Level.Information, "Copenhagen", "Hello World");
            _weatherMock.Setup(x => x.GetWeatherForCityAsync(command.City, CancellationToken.None))
                .ReturnsAsync(weatherInfo);

            _publisherMock.Setup(x => x.PublishMessageAsync("test", "", notification, CancellationToken.None)).Verifiable();

            // Act
            var result = await _sut.Handle(command, new CancellationToken());

            // Assert

            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
        }


        [Fact]
        public async Task PublishNotificationCommand_ShouldFail_WhenInvalidRequest()
        {
            // Arrange

            var notification = new Notification("test", DateTimeOffset.Now, 12.3, "Copenhagen", "Chilly", Level.Critical);

            var command = new PublishNotificationCommand(Level.Information, "Copenhagen", "Hello World");
            _weatherMock.Setup(x => x.GetWeatherForCityAsync(command.City, CancellationToken.None))
                .ThrowsAsync(new WeatherApiException(HttpStatusCode.BadRequest, "Not found"));

            _publisherMock.Setup(x => x.PublishMessageAsync("test", "", notification, CancellationToken.None)).Verifiable();

            // Act & Assert
            await Assert.ThrowsAsync<WeatherApiException>(async () =>
            {
                await _sut.Handle(command, new CancellationToken());
            });


        }
    }
}