﻿using System.Threading;
using System.Threading.Tasks;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Domain.Core.v1.Exceptions;
using Notifier.Publisher.Infrastructure.Repositories.v1.Weather;
using Notifier.Publisher.IntegrationTests.Repositories.v1.Mocks;
using Xunit;

namespace Notifier.Publisher.IntegrationTests.Repositories.v1
{
    public class WeatherRepositoryTests
    {
        private readonly IWeatherRepository _sut;

        public WeatherRepositoryTests()
        {
            IConfigurationManagerRepository configuration = new MockConfigurationWrapperRepository();
            _sut = new WeatherRepository(configuration);
        }

        [Fact]
        public async Task GetWeatherForCityAsync_ShouldSucceed_WhenCityIsValid()
        {
            // Arrange

            var city = "Copenhagen";

            // Act

            var result = await _sut.GetWeatherForCityAsync(city, CancellationToken.None).ConfigureAwait(false);

            // Assert

            Assert.NotNull(result);
            Assert.Equal("Copenhagen", result.Location.Name);
            Assert.Equal("Hovedstaden", result.Location.Region);
            Assert.Equal("Denmark", result.Location.Country);
        }

        [Fact]
        public async Task GetWeatherForCityAsync_ShouldThrowWeatherApiException_WhenCityIsInValid()
        {
            // Arrange

            var city = "Copegen";

            // Act && Assert
            await Assert.ThrowsAsync<WeatherApiException>(async () =>
            {
                var result = await _sut.GetWeatherForCityAsync(city, CancellationToken.None).ConfigureAwait(false);
            });
        }
    }
}
