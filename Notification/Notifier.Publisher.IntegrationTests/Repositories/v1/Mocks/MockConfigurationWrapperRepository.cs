﻿using System.Collections.Generic;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;

namespace Notifier.Publisher.IntegrationTests.Repositories.v1.Mocks
{
    public class MockConfigurationWrapperRepository : IConfigurationManagerRepository
    {
        private readonly Dictionary<string, string> _configuration;
        public MockConfigurationWrapperRepository()
        {
            _configuration = new Dictionary<string, string>()
            {
                {"WeatherApi:Uri", "http://api.weatherapi.com"},
                {"WeatherApi:Key", "6a5f2e3e34384ebfa4b135235213005"},
                {"RabbitMq:Hostname", "rabbitmq"},
                {"RabbitMq:Port", "5672"},
                {"RabbitMq:Username", "admin"},
                {"RabbitMq:Password", "admin1234"},
                {"RabbitMq:Exchange", "Notifier.Fanout"},
            };
        }

        public string GetValue(string key)
        {
            return _configuration.GetValueOrDefault(key);
        }

        public bool HasKey(string key)
        {
            return _configuration.ContainsKey(key);
        }
    }
}
