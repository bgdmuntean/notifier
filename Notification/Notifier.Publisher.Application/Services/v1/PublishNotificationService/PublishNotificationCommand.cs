﻿using MediatR;
using Notifier.Publisher.Contracts.Responses.v1;
using Notifier.Publisher.Domain.Models.v1;

namespace Notifier.Publisher.Application.Services.v1.PublishNotificationService
{
    public class PublishNotificationCommand : IRequest<ResponseObject<PublishNotificationResponse>>
    {
        public PublishNotificationCommand(Level level, string city, string message)
        {
            Level = level;
            City = city;
            Message = message;
        }

        public Level Level { get; set; }
        public string City { get; set; }
        public string Message { get; set; }
    }
}