﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.RabbitMq;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Application.Utils.v1.Constants;
using Notifier.Publisher.Application.Utils.v1.Extensions;
using Notifier.Publisher.Contracts.Responses.v1;
using Notifier.Publisher.Domain.Models.v1;

namespace Notifier.Publisher.Application.Services.v1.PublishNotificationService
{
    public class PublishNotificationCommandHandler : IRequestHandler<PublishNotificationCommand, ResponseObject<PublishNotificationResponse>>
    {
        private readonly IWeatherRepository _weatherRepository;
        private readonly IPublisherRepository _publisherRepository;
        private readonly IConfigurationManagerRepository _configuration;
        private readonly string _topic;

        public PublishNotificationCommandHandler(IPublisherRepository publisherRepository, IWeatherRepository weatherRepository, IConfigurationManagerRepository configuration)
        {
            _publisherRepository = publisherRepository;
            _weatherRepository = weatherRepository;
            _configuration = configuration;

            _topic =_configuration.GetValue(RabbitMq.Exchange);
        }

        public async Task<ResponseObject<PublishNotificationResponse>> Handle(PublishNotificationCommand request, CancellationToken cancellationToken)
        {
            var weatherResponse = await _weatherRepository
                .GetWeatherForCityAsync(request.City, cancellationToken)
                .ConfigureAwait(false);

            var notification = new Notification(request.Message, DateTimeOffset.Now,
                weatherResponse.Current.Temperature, weatherResponse.Location.Name,
                weatherResponse.Current.Condition.Text, request.Level);

            await _publisherRepository
                .PublishMessageAsync(_topic, "", notification.ToResponseObject(), cancellationToken)
                .ConfigureAwait(false);

            return new ResponseObject<PublishNotificationResponse>(notification.ToResponseObject());
        }
    }
}