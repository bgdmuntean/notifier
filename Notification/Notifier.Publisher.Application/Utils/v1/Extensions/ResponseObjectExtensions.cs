﻿using Notifier.Publisher.Contracts.Responses.v1;
using Notifier.Publisher.Domain.Models.v1;

namespace Notifier.Publisher.Application.Utils.v1.Extensions
{
    public static class ResponseObjectExtensions
    {
        public static PublishNotificationResponse ToResponseObject(this Notification notification)
        {
            return new PublishNotificationResponse(notification.Message, notification.Timestamp, notification.TemperatureCelsius.Value, notification.City, notification.Condition, notification.Level.ToString());
        }
    }
}