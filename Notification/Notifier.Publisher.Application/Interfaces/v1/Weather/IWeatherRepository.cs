﻿using System.Threading;
using System.Threading.Tasks;
using Notifier.Publisher.Contracts.WeatherApi.v1;

namespace Notifier.Publisher.Application.Interfaces.v1.Weather
{
    public interface IWeatherRepository
    {
        Task<WeatherInformation> GetWeatherForCityAsync(string cityName, CancellationToken token);
    }
}