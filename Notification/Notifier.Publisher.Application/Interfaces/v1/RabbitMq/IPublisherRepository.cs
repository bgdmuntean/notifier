﻿using System.Threading;
using System.Threading.Tasks;

namespace Notifier.Publisher.Application.Interfaces.v1.RabbitMq
{
    public interface IPublisherRepository
    {
        Task PublishMessageAsync<T>(string topic, string routingKey, T message, CancellationToken token);
    }
}