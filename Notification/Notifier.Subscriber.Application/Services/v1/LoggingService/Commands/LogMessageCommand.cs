﻿using MediatR;
using Notifier.Subscriber.Domain.Models.v1;

namespace Notifier.Subscriber.Application.Services.v1.LoggingService.Commands
{
    public class LogMessageCommand : IRequest<Unit>
    {
        public LogMessageCommand()
        {
        }
        public LogMessageCommand(Notification notification)
        {
            Notification = notification;
        }
        public Notification Notification { get; set; }
    }
}