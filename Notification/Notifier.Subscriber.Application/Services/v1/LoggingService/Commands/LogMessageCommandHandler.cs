﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Notifier.Subscriber.Application.Interfaces.v1.Logger;

namespace Notifier.Subscriber.Application.Services.v1.LoggingService.Commands
{
    public class LogMessageCommandHandler : IRequestHandler<LogMessageCommand, Unit>
    {
        private readonly ILoggerRepository _loggerRepository;

        public LogMessageCommandHandler(ILoggerRepository loggerRepository)
        {
            _loggerRepository = loggerRepository;
        }

        public async Task<Unit> Handle(LogMessageCommand request, CancellationToken cancellationToken)
        {
            await _loggerRepository
                .LogAsync(request.Notification.GetLoggingInfo(), request.Notification.Level, cancellationToken)
                .ConfigureAwait(false);

            return Unit.Value;
        }
    }
}