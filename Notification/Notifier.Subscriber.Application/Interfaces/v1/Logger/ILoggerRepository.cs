﻿using System.Threading;
using System.Threading.Tasks;
using Notifier.Subscriber.Domain.Models.v1;

namespace Notifier.Subscriber.Application.Interfaces.v1.Logger
{
    public interface ILoggerRepository
    {
        Task LogAsync(string formattedMessage, Level level, CancellationToken token);
    }
}