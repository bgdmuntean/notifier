﻿namespace Notifier.Subscriber.Application.Interfaces.v1.ConfigurationWrapper
{
    public interface IConfigurationManagerRepository
    {
        string GetValue(string key);
        bool HasKey(string key);
    }
}