﻿using FluentValidation;
using Notifier.Publisher.Contracts.Requests.v1;

namespace Notifier.Publisher.API.Models.v1.Validators
{
    public class PublishNotificationRequestValidator : AbstractValidator<PublishNotificationRequest>
    {
        public PublishNotificationRequestValidator()
        {
            RuleFor(_ => _.City)
                .NotEmpty()
                .WithMessage("[City] is missing");

            RuleFor(_ => _.Level)
                .NotNull()
                .WithMessage("[Level] is null");

            RuleFor(_ => _.Message)
                .NotEmpty()
                .WithMessage("[Message] is missing");
        }
    }
}
