﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Notifier.Publisher.API.Core.v1.Filters;

namespace Notifier.Publisher.API.Core.v1.Configurations
{
    internal static class ConfigureFluentValidationExtensions
    {
        internal static void ConfigureFluentValidation(this IServiceCollection services)
        {
            services.AddMvcCore(_ =>
                    _.Filters.Add(new ValidationFilter())
                )
                .AddFluentValidation(_ =>
                    _.RegisterValidatorsFromAssemblyContaining<Startup>());

        }
    }
}