﻿using Microsoft.Extensions.DependencyInjection;
using Notifier.Publisher.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Publisher.Application.Interfaces.v1.RabbitMq;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Infrastructure.Repositories.v1.ConfigurationWrapper;
using Notifier.Publisher.Infrastructure.Repositories.v1.RabbitMqPublisher;
using Notifier.Publisher.Infrastructure.Repositories.v1.Weather;

namespace Notifier.Publisher.API.Core.v1.Configurations
{
    internal static class ConfigureRepositoriesExtensions
    {
        internal static void ConfigureRepositoryRegistration(this IServiceCollection services)
        {
            services.AddScoped<IConfigurationManagerRepository, ConfigurationManagerRepository>();
            services.AddScoped<IWeatherRepository, WeatherRepository>();
            services.AddScoped<IPublisherRepository, RabbitMqPublisherRepository>();
        }
    }
}