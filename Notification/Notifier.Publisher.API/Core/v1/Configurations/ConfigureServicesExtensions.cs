﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Notifier.Publisher.Application.Services.v1.PublishNotificationService;
using Notifier.Publisher.Contracts.Responses.v1;

namespace Notifier.Publisher.API.Core.v1.Configurations
{
    internal static class ConfigureServicesExtensions
    {
        internal static void ConfigureServicesRegistration(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddTransient<IRequestHandler<PublishNotificationCommand, ResponseObject<PublishNotificationResponse>>, PublishNotificationCommandHandler>();
        }
    }
}