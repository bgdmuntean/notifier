﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Notifier.Publisher.Application.Interfaces.v1.Weather;
using Notifier.Publisher.Domain.Core.v1;

namespace Notifier.Publisher.API.Controllers.v1
{
    [Route("api/v1/health-check")]
    [Produces("application/json")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
        private readonly IWeatherRepository _weatherRepository;

        public HealthCheckController(IWeatherRepository weatherRepository)
        {
            _weatherRepository = weatherRepository;
        }


        /// <summary>
        /// Check the health of services
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     HEAD api/v1/health-check
        ///
        /// </remarks>
        /// <param name="token">Cancellation token</param>
        /// <response code="204">Returns an empty response - all is in order</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpHead]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<object>> HealthCheck(CancellationToken token)
        {
            var weatherResponse = await _weatherRepository
                .GetWeatherForCityAsync("Copenhagen", token)
                .ConfigureAwait(false);

            return NoContent();
        }
    }
}
