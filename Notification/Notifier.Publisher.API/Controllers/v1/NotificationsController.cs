﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Notifier.Publisher.API.Utils.v1.Extensions;
using Notifier.Publisher.Contracts.Requests.v1;
using Notifier.Publisher.Contracts.Responses.v1;
using Notifier.Publisher.Domain.Core.v1;

namespace Notifier.Publisher.API.Controllers.v1
{
    [Route("api/v1/notifications")]
    [Produces("application/json")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public NotificationsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Publish Message
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/v1/notifications
        ///
        /// </remarks>
        /// <param name="request">Publish Notification Request</param>
        /// <param name="token">Cancellation token</param>
        /// <response code="200">Returns a response object with a notification response</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<PublishNotificationResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<PublishNotificationResponse>>> PublishMessage([FromBody, BindRequired] PublishNotificationRequest request, CancellationToken token)
            => Ok(await _mediator
                    .Send(request.ToCommand(), token)
                    .ConfigureAwait(false)
                );
    }
}