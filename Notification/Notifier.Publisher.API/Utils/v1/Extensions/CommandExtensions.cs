﻿using Notifier.Publisher.Application.Services.v1.PublishNotificationService;
using Notifier.Publisher.Contracts.Requests.v1;

namespace Notifier.Publisher.API.Utils.v1.Extensions
{
    public static class CommandExtensions
    {
        public static PublishNotificationCommand ToCommand(this PublishNotificationRequest request)
        {
            return new PublishNotificationCommand(request.Level, request.City, request.Message);
        }   
    }
}