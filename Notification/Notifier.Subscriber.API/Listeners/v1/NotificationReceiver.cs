﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Notifier.Subscriber.API.Utils.v1.Constants;
using Notifier.Subscriber.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Subscriber.Application.Services.v1.LoggingService.Commands;
using Notifier.Subscriber.Domain.Models.v1;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Notifier.Subscriber.API.Listeners.v1
{
    public class NotificationReceiver : BackgroundService
    {
        private readonly IConfigurationManagerRepository _configuration;
        private readonly IMediator _mediator;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;

        public NotificationReceiver(IConfigurationManagerRepository configuration, IMediator mediator)
        {
            _configuration = configuration;
            _mediator = mediator;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            await Task.Delay(5000, cancellationToken);
            var connectionFactory = GetConnectionFactory();
            BindQueueToExchange(connectionFactory);
            await base.StartAsync(cancellationToken).ConfigureAwait(false);
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += OnMessageReceived;

            _channel.BasicConsume(queue: _queueName, autoAck: false, consumer: consumer);

            await Task.CompletedTask;
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _connection.Close();
        }

        private async Task OnMessageReceived(object bc, BasicDeliverEventArgs ea)
        {
            try
            {
                var notification = ExtractObjectFromMessage(ea);

                await _mediator.Send(new LogMessageCommand(notification))
                    .ConfigureAwait(false);

                _channel.BasicAck(ea.DeliveryTag, false);
            }
            catch (JsonException exception)
            {
                _channel.BasicNack(ea.DeliveryTag, false, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static Notification ExtractObjectFromMessage(BasicDeliverEventArgs ea)
        {
            var message = Encoding.UTF8.GetString(ea.Body.ToArray());
            var notification = JsonConvert.DeserializeObject<Notification>(message);
            return notification;
        }

        private void BindQueueToExchange(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();

            _queueName = _channel.QueueDeclare().QueueName;

            _channel.QueueBind(queue: _queueName,
                exchange: "Notifier.Fanout",
                routingKey: "");
        }

        private ConnectionFactory GetConnectionFactory()
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = _configuration.GetValue(RabbitMq.Hostname),
                Port = Convert.ToInt32(_configuration.GetValue(RabbitMq.Port)),
                UserName = _configuration.GetValue(RabbitMq.Username),
                Password = _configuration.GetValue(RabbitMq.Password),
                DispatchConsumersAsync = true
            };
            return connectionFactory;
        }
    }
}
