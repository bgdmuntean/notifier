using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Notifier.Subscriber.API.Core.v1.Configurations;
using Notifier.Subscriber.API.Core.v1.Middleware;
using Notifier.Subscriber.API.Listeners.v1;
using Notifier.Subscriber.API.Utils.v1;
using Notifier.Subscriber.Domain.Core.v1;
using Serilog;

namespace Notifier.Subscriber.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            ConfigurationManager.AppSettings = Configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.ConfigureRepositoryRegistration();
            services.ConfigureServicesRegistration();
            services.ConfigureSwagger();

            services.AddHostedService<NotificationReceiver>();

            ConfigureSerilogLogger(services);
        }

        private static void ConfigureSerilogLogger(IServiceCollection services)
        {
            var logger = SerilogLogger.Define();
            services.AddLogging(lb => lb.AddSerilog(logger));
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseDeveloperExceptionPage();
            }

            app.UseNativeGlobalExceptionHandler();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Subscriber API");
                c.EnableFilter();
                c.DisplayOperationId();
                c.DisplayRequestDuration();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
