﻿using Serilog;
using Serilog.Core;

namespace Notifier.Subscriber.API.Utils.v1
{
    public static class SerilogLogger
    {
        private const string ContainerName = "instance-logs";

        public static Logger Define()
        {
            return new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();
        }
    }
}
