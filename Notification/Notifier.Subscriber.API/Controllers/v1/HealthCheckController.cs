﻿using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Notifier.Subscriber.Domain.Core.v1;

namespace Notifier.Subscriber.API.Controllers.v1
{
    [Route("api/v1/health-check")]
    [Produces("application/json")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {



        /// <summary>
        /// Check the health of services
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     HEAD api/v1/health-check
        ///
        /// </remarks>
        /// <param name="token">Cancellation token</param>
        /// <response code="204">Returns an empty response - all is in order</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpHead]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public ActionResult HealthCheck(CancellationToken token)
        {
            return NoContent();
        }
    }
}
