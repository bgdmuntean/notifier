﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Notifier.Subscriber.Domain.Core.v1;
using Notifier.Subscriber.Domain.Core.v1.Exceptions;

namespace Notifier.Subscriber.API.Core.v1.Middleware
{
    internal static class ExceptionMiddleware
    {
        internal static IApplicationBuilder UseNativeGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;

                    var errorResponse = exception switch
                    {
                        BaseException apiException => new ErrorResponse(apiException),
                        _ => new ErrorResponse(exception)
                    };
                    context.Response.StatusCode = (int)errorResponse.StatusCode;
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(errorResponse.ToJsonString());
                });
            });

            return app;
        }
    }
}