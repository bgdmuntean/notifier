﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Notifier.Subscriber.Application.Services.v1.LoggingService.Commands;

namespace Notifier.Subscriber.API.Core.v1.Configurations
{
    internal static class ConfigureServicesExtensions
    {
        internal static void ConfigureServicesRegistration(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<IRequestHandler<LogMessageCommand, Unit>, LogMessageCommandHandler>();
        }
    }
}