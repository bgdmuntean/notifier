﻿using Microsoft.Extensions.DependencyInjection;
using Notifier.Subscriber.Application.Interfaces.v1.ConfigurationWrapper;
using Notifier.Subscriber.Application.Interfaces.v1.Logger;
using Notifier.Subscriber.Infrastructure.Repositories.v1.ConfigurationWrapper;
using Notifier.Subscriber.Infrastructure.Repositories.v1.SerilogLogger;

namespace Notifier.Subscriber.API.Core.v1.Configurations
{
    internal static class ConfigureRepositoriesExtensions
    {
        internal static void ConfigureRepositoryRegistration(this IServiceCollection services)
        {
            services.AddSingleton<IConfigurationManagerRepository, ConfigurationManagerRepository>();
            services.AddTransient<ILoggerRepository, SerilogLoggerRepository>();
        }
    }
}