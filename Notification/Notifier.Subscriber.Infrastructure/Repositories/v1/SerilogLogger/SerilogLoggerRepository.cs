﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Notifier.Subscriber.Application.Interfaces.v1.Logger;
using Notifier.Subscriber.Domain.Models.v1;


namespace Notifier.Subscriber.Infrastructure.Repositories.v1.SerilogLogger
{
    public class SerilogLoggerRepository : ILoggerRepository
    {
        private readonly ILogger<SerilogLoggerRepository> _logger;

        public SerilogLoggerRepository(ILogger<SerilogLoggerRepository> logger)
        {
            _logger = logger;
        }

        public async Task LogAsync(string formattedMessage, Level level, CancellationToken token)
        {
            switch (level)
            {
                case Level.Information:
                    _logger.LogInformation(formattedMessage);
                    break;
                case Level.Critical:
                    _logger.LogCritical(formattedMessage);
                    break;
                case Level.Warning:
                    _logger.LogWarning(formattedMessage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}